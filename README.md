# TrustUs - client.c #
## What is it ? ##
TrustUs - client.c is a standalone executable that can be drop and run on an android device, and add ca-cert file to the system's ca-cert chain
## What it does? ##
this executable will connect back home to the a specific
IP and Port which are defined in it's code, when it succeeded  to connect back to server, the server send the name and the buffer of the ca-cert file
then it saves the ca-cert file to a specific folder change the file's owner and mask-mode
## Dependencies: ##
Must be run on an android device as root

## How to make it work? ##
just run it
make sure you run it as root by:
chown root:root PATH_TO_FILE
make sure you set the file execute premonitions by:
chmod 777


## Compiling: ##
Download the android NDK for Linux - [here](https://developer.android.com/ndk/downloads/index.html)
extract it to any folder you like
On the android-ndk root folder there will be a folder called "samples" from there copy the folder "Hello-jni" to the folder sources on the android-ndk root folder
change the new folder you just copy to the any name you like
now in the new folder you just created enter the sub folder "jni"
there will be a configuration file called "Android.mk" replace its content with this:
_____________________________________________________
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CFLAGS += -fPIE
LOCAL_LDFLAGS += -fPIE -pie
LOCAL_MODULE    := Client
LOCAL_SRC_FILES := client.c
_____________________________________________________

LOCAL_MODULE - You can change it, it will be the name of the executable after compilation
LOCAL_SRC_FILES - You can change it, it is the name of the source file in the current folder(jni) to compile


now all you have to do is to copy the code from here to the LOCAL_SRC_FILES file you choose
and run from the jni folder:
../../../ndk-build

# adjust_ca.sh #
## What is it ? ##
a bash script for converting a ca-cert created by openssl(or any other framework for crating ca cert) to a ca-cert file in a format which the android system accept to get
## What it does? ##
It's hash the ca-cert and rename it with that hash, it also convert it to a pem format and put both the crt buf and the pem buf to the same file
## Dependencies: ##
-Work only on Linux
-openssl
-an existing ca-cert file in crt fomrat

## How to make it work? ##
adjust_ca.sh <your_ca_cert_file_path>


# TrustUs - Server.py #
## What is it ? ##
A python server designed to send the adjust-ce-cert file to the TrustUs - client
## What it does? ##
It sets up a server waiting for connections from android devices then it sends the name of the adjust-ce-cert and it's contect
## Dependencies: ##
Windows and Linux:
       -Python 2.7

## How to make it work? ##
python Server.py -c <path-to-adjust-ca-cert-file>
for much more Server.py -h