#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/mount.h>

#define IP "192.168.1.39"
#define PORT 1337
#define FORMAT_SYSTEM_CA_CERT_FILDER "/system/etc/security/cacerts/%s"
#define LINUX_MAX_PATH 4096
#define BYTES_TO_READ 8192

/**
	The function gets the socket descriptor
	then it tries to connect the server and read the CA certificate file name and its buffer
	then it chown the file to SYSTEM and set its permissions to 644(rw, r, r) and return
**/
int save_ca_cert_file(int sockfd);
uint32_t get_buf_length(int sockfd);
int init_connection(const char* ip, const int port);

int main( int argc, char* argv[])
{
	int sockfd = init_connection(IP, PORT);
	if (1 == sockfd)
	{
		return 0;
	}
	if (1 == save_ca_cert_file(sockfd))
	{
		close(sockfd);
        	return 0;
	}
	close(sockfd);
	return 0;
}
int init_connection(const char* ip, const int port)
{
	printf("[*] Creating Socket\n");
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (0 > sockfd)
	{
		printf("[*] socket() Failed\n");
		return 1;
	}
	struct hostent* ip_addr = { 0 };
	ip_addr = gethostbyname(ip); //This function can also get hostnames like google.com
	struct sockaddr_in sockfd_addr = { 0 };
	sockfd_addr.sin_family = AF_INET;
	bcopy((char*)ip_addr->h_addr, (char*)&sockfd_addr.sin_addr.s_addr,
		ip_addr->h_length);
	sockfd_addr.sin_port = htons(port);
	printf("[*] Connecting to IP : %s on port : %d\n", ip, port);
	int err = connect(sockfd, (struct sockaddr*)&sockfd_addr, sizeof(sockfd_addr));
	if (0 > err)
	{
		printf("[*] connect() Failed\n");
		close(sockfd);
		return 1;
	}
	printf("[*] Connected\n");
	return sockfd;
}

uint32_t get_buf_length(int sockfd)
{
	uint8_t buf[4];
	if (recv(sockfd, buf, sizeof buf, 0) == 4)
	{
		return(((uint32_t) buf[0] << 24) |
                           ((uint32_t) buf[1] << 16) |
                           ((uint32_t) buf[2] << 8) |
                           buf[3]);
	}
	else
	{
		return 0;
	}
}

int save_ca_cert_file(int sockfd)
{
	uint32_t ca_cert_name_len = get_buf_length(sockfd);
	if (0 == ca_cert_name_len)
	{
		return 1;
	}
	char* ca_cert_name = malloc(ca_cert_name_len);
	recv(sockfd, ca_cert_name, ca_cert_name_len, MSG_WAITALL);
	char* new_ca_cert_path = malloc(LINUX_MAX_PATH);
	sprintf(new_ca_cert_path, FORMAT_SYSTEM_CA_CERT_FILDER, ca_cert_name);
	free(ca_cert_name);
	uint32_t ca_cert_buf_len = get_buf_length(sockfd);
	if (0 == ca_cert_buf_len)
	{
		return 1;
	}
	char* ca_cert_buf = malloc(ca_cert_buf_len);
	recv(sockfd, ca_cert_buf, ca_cert_buf_len, MSG_WAITALL);
	if (-1 == mount("none", "/system", "none", MS_REMOUNT, NULL))
	{
		return 1;
	}
	FILE* file = fopen(new_ca_cert_path, "wb");
	if (NULL == file)
	{
		printf("[*] fopen() Failed\n");
		return 1;
	}
	fwrite(ca_cert_buf, ca_cert_buf_len, ca_cert_buf_len, file);
	fclose(file);
    struct passwd *pwd;
    pwd = getpwnam("system");
    if (NULL == pwd)
    {
        return 1;
    }
    if (-1 == chown(new_ca_cert_path, pwd->pw_uid, pwd->pw_gid))
    {
        return 1;
    }
	if (-1 == chmod(new_ca_cert_path, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH))
	{
        return 1;
	}
	if (-1 == mount("none", "/system", "none", MS_RDONLY | MS_REMOUNT, NULL))
	{
		return 1;
	}
    return 0;
}
