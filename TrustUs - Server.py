from socket import *
import argparse
import os
import time
import struct

MAX_CLIENTS = 100 

def valid_ip(ip):
    try:
        inet_aton(ip)
        return 3 == ip.count(".")
    except error:
        return False
        
def server_mode(ip, port, ca_cert_buf, ca_cert_filename):
    try:
        s = socket()
        s.bind((ip, port))
        print ("[*] Listening on %s:%d\n[*] Waiting for clients" % (ip, port))
        if ("win32" == os.sys.platform):
            print("[*] Press Ctrl + Break to stop server")
        elif ("linux" in os.sys.platform):
            print("[*] Press Ctrl + C to stop server")
        while True:
            s.listen(MAX_CLIENTS)
            client, client_ip = s.accept()
            client_ip = client_ip[0]
            print("[*] Got connection from %s" % client_ip)
            buf = struct.pack(">I", len(ca_cert_filename)) + ca_cert_filename  #We send the file name because the ca_cert must be save in the remote device with the same name
            client.send(buf)
            print("[*] Sent %d bytes for %s" % (len(buf), client_ip))
            buf = struct.pack(">I", len(ca_cert_buf)) + ca_cert_buf
            client.send(buf)
            client.close()
            print("[*] Sent %d bytes for %s" % (len(buf), client_ip))
    except KeyboardInterrupt:
        s.close()
        print("[*] Closed socket")
        exit()
    except:
        s.close()
        raise

def main():
    if (not "win32" == os.sys.platform and not "linux" in os.sys.platform):
        print("[*] Only support Windows or Linux")
        exit(1)
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--listen_on", type = str, help = "The IP address  \
                                                                                                to listen on, Default: 0.0.0.0", default = "0.0.0.0")
    parser.add_argument("-p", "--port", type = int, help = "The port  \
                                                                                                to listen on, Default: 1337", default = 1337)
    parser.add_argument("-c", "--ca_cert", required = True, type = str, help = "The Path  \
                                                                                                of the CA certificate after adjust_ca.sh have been modify it", default = "")
    args = parser.parse_args()
    if (not valid_ip(args.listen_on)):
        print ("[*] Bad IP")
        exit(1)
    if (not 0 < args.port <= 65535):
        print ("[*] Bad Port must be between 1 to 65535")
        exit(1)
    if (not os.path.exists(args.ca_cert)):
        print ("[*] The file %s doen't exists" % args.ca_cert)
        exit(1)
    else:
        ca_cert_filename = os.path.basename(args.ca_cert)
        with open(args.ca_cert, "rb") as f:
            ca_cert_buf = f.read()
    server_mode(args.listen_on, args.port, ca_cert_buf, ca_cert_filename)

if ("__main__" == __name__):
    main()